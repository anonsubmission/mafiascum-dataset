# The Mafiascum Dataset

The [Mafiascum](https://forum.mafiascum.net) Dataset is a collection of over 700 [games of Mafia](https://wiki.mafiascum.net/index.php?title=Game_of_Mafia) where players were randomly assigned deceptive and non-deceptive roles.
Parsable alignment distributions are available for the vast majority of the games, making the dataset suitable for training deception classifiers.  To download the dataset, clone this repository using git or download the ZIP file [here](https://bitbucket.org/anonsubmission/mafiascum-dataset/get/HEAD.zip).

The benchmark described in the accompanying paper is implemented in `benchmark-template.py`.

## Reproduction

To reproduce the paper's results, run `combined.py`. This script preprocesses the JSON data included in this repository into 10,000 text documents. These are cached in docs.pkl.
Each of the text documents includes all posts by a single player in a single game, and is annotated with a binary `scum` label, denoting whether the player is deceptive in that game.
These documents are then transformed into feature sets, by extracting hand-picked features in one case, by taking the average word vector in another case, and by doing both in yet another case.
A fresh linear SVM-ish model is cross-validated on all three of these feature sets. PR curves and ROC curves are saved to .png files, although they are overwritten on every iteration.

If USE_CACHED_DOCS is set to false, `combined.py` requires downloading the English wiki word vectors from Fasttext, and the Twitter/Wiki 200d/Wiki 300d word vectors from GloVe. pip install gensim, then run:

```
python -m gensim.scripts.glove2word2vec -i glove.twitter.27B.200d.txt -o w2v.twitter.txt
python -m gensim.scripts.glove2word2vec -i glove.6B.200d.txt -o w2v.wiki.txt
python -m gensim.scripts.glove2word2vec -i glove.6B.300d.txt -o w2v.wiki200d.txt
