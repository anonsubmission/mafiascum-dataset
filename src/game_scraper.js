forum = 52

scraper = function(){
  return {
    href: this.querySelector('.topictitle').href,
    id: +this.querySelector('.topictitle').href.match(/t=[0-9]+/)[0].slice(2),
    title: this.querySelector('.topictitle').textContent,
    mod: this.querySelector('[href*=memberlist]').textContent,
    inserted_at: +new Date(this.querySelector('dt:first-child').textContent.match(/» .+/)[0].slice(2)),
    posts: +this.querySelector('.posts').textContent.match(/[0-9]+/)[0],
    forum
  }
}

artoo.ajaxSpider(function(i){return "https://forum.mafiascum.net/viewforum.php?f=" + forum + "&start=" + i * 100}, {
  scrape: {iterator: '[class=forumbg] .row', data: scraper},
  throttle: 3000, limit: 6, concat: true
}, x => console.log(games=x))