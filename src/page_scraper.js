pages=[];
for (g of games)
	for (var i = 0; i < g.posts; i+=200)
		pages.push({href: g.href + "&view=print&ppp=200&start=" + i, game_id: g.id, start: i})

pageScraper = function(data, i, acc) {
	var root = document.implementation.createHTMLDocument('preview').createElement('html')
	root.innerHTML = data
	root.querySelectorAll("blockquote").forEach(x => x.remove())
	return [...root.querySelectorAll('.post')].map((p, post_i) => (
		{
			author: p.querySelector('.author strong').textContent,
			content: p.querySelector('.content').innerText,
			inserted_at: +new Date(p.querySelector('.date strong').textContent),
			game_id: pages[i].game_id,
			post_no: pages[i].start + post_i
		}
	))
}

artoo.ajaxSpider(pages.map(x => x.href), {process: pageScraper, concat: true, throttle: 1000},
		 x => console.log(posts=x))