from sklearn import svm, linear_model, model_selection, metrics
import pandas as pd
import numpy as np

docs = pd.read_pickle('docs.pkl', compression='gzip')

# you probably want to do something with docs['content'] here instead

columns = "wc24h or_ratio tpp_ratio spp_ratio fpp_ratio but_ratio token_length unique_tokens message_length sentence_length msg24h anger_ratio sensory_ratio cog_ratio insight_ratio motion_ratio not_ratio quant_ratio neg_em_ratio tent_ratio".split()

X = docs[columns].values
y = docs['scum'].values

y_pred = np.zeros(len(y))

for train_index, test_index in model_selection.StratifiedShuffleSplit(20).split(X, y):
    X_test = X[test_index]
    X_train = X[train_index]
    y_test = y[test_index]
    y_train = y[train_index]

    pipeline = linear_model.LogisticRegression(class_weight='balanced', solver='lbfgs', max_iter=1000)
    pipeline.fit(X_train, y_train)
    y_pred[test_index] = pipeline.predict_proba(X_test)[:,1]

score = metrics.average_precision_score(y, y_pred)

print(score)
