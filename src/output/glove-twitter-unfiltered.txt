-*- mode: compilation; default-directory: "~/thesis/" -*-
Comint started at Thu Oct  5 15:19:26

python fasttext.py
attempted to convert 77250487 words to word embeddings, failed on 940577 
success ratio = 0.9878243227126839
{'fit_time': array([ 3.43585944,  3.44072938,  3.36119986]), 'score_time': array([ 0.06467366,  0.01685739,  0.01990294]), 'test_f1': array([ 0.35649297,  0.37228372,  0.36682343]), 'train_f1': array([ 0.41666667,  0.40946591,  0.42068106]), 'test_accuracy': array([ 0.53676689,  0.54407385,  0.55896367]), 'train_accuracy': array([ 0.59142347,  0.59505732,  0.584636  ]), 'test_precision': array([ 0.26555761,  0.27615572,  0.27784974]), 'train_precision': array([ 0.31470777,  0.31266578,  0.31401116]), 'test_recall': array([ 0.54213836,  0.57106918,  0.53962264]), 'train_recall': array([ 0.6163522 ,  0.59308176,  0.63710692]), 'test_roc_auc': array([ 0.55578057,  0.5591406 ,  0.56347637]), 'train_roc_auc': array([ 0.63423078,  0.63608514,  0.63618677]), 'test_average_precision': array([ 0.26898843,  0.26228474,  0.27987569]), 'train_average_precision': array([ 0.33842641,  0.34711229,  0.3332751 ])}

Inferior Python finished at Thu Oct  5 15:24:59
