-*- mode: compilation; default-directory: "~/thesis/" -*-
Comint started at Thu Oct  5 12:52:27

python classify.py
0.251726121979
{'fit_time': array([ 1.08260512,  1.20526338,  1.08899832]), 'score_time': array([ 0.03813434,  0.01032972,  0.0104177 ]), 'test_f1': array([ 0.38648649,  0.37871287,  0.41344956]), 'train_f1': array([ 0.45771144,  0.47885572,  0.43911439]), 'test_accuracy': array([ 0.608283  ,  0.56686799,  0.59326425]), 'train_accuracy': array([ 0.62365127,  0.63832542,  0.60655738]), 'test_precision': array([ 0.31919643,  0.29651163,  0.32421875]), 'train_precision': array([ 0.35902439,  0.37560976,  0.34261036]), 'test_recall': array([ 0.48972603,  0.5239726 ,  0.57044674]), 'train_recall': array([ 0.63121784,  0.66037736,  0.61130137]), 'test_roc_auc': array([ 0.59968637,  0.58428924,  0.61447421]), 'train_roc_auc': array([ 0.67165221,  0.68788492,  0.67173157]), 'test_average_precision': array([ 0.33939343,  0.31350764,  0.33581518]), 'train_average_precision': array([ 0.39751672,  0.41000294,  0.41408193])}

Inferior Python finished at Thu Oct  5 12:52:35
