from gensim.models.wrappers import FastText
from gensim.models import KeyedVectors
import gensim
import pandas as pd
import re
import numpy
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm
from sklearn import dummy
from sklearn.metrics import precision_recall_curve
from sklearn.naive_bayes import MultinomialNB

assert gensim.models.doc2vec.FAST_VERSION > -1

FASTTEXT = True
USE_DOCS = True
dataset = "wiki"

# python -m gensim.scripts.glove2word2vec -i glove.twitter.27B.200d.txt -o w2v.twitter.txt
x = FastText.load_fasttext_format(dataset+".en") if FASTTEXT else KeyedVectors.load_word2vec_format('w2v.'+dataset+'.txt', binary=False)
total_count = missing_count = 0

def V(w):
    global total_count, missing_count
    try:
        total_count += 1
        return x[w]
    except KeyError:
        missing_count += 1
        return None

docs = pd.read_pickle('docs.pkl' if USE_DOCS else 'labeled_posts.pkl', compression='gzip')

# exclude numbers since pre-trained model does not have ngrams for them
docs['words'] = docs['content'].apply(lambda c: re.findall(r"[^\W\d]+", c.lower(), re.UNICODE))
docs['vector'] = docs['words'].apply(lambda ws: numpy.mean([V(word) for word in ws if not V(word) is None], axis=0))

print("attempted to convert", total_count, "words to word embeddings, failed on", missing_count,
      "\nsuccess ratio =", (total_count - missing_count) / total_count)

model = svm.LinearSVC(class_weight='balanced')
# model = MultinomialNB()
# model = dummy.DummyClassifier()
# avg precision = AU PRC
scores = cross_validate(model, numpy.stack(docs['vector'].values), docs['scum'].values.astype('bool'), cv=StratifiedKFold(n_splits=10),
                        scoring=['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'])
print(scores)

if USE_DOCS:
    docs.to_pickle('fasttext-'+dataset+'-avg.pkl' if FASTTEXT else 'glove-'+dataset+'-avg.pkl', compression='gzip')
else:
    docs.to_pickle('fasttext-'+dataset+'-avg-posts.pkl' if FASTTEXT else 'glove-'+dataset+'-avg-posts.pkl', compression='gzip')
