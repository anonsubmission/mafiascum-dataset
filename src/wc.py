import pandas as pd
import re

USE_DOCS = True

cnt = lambda l, words: sum(l.count(w) for w in words)

docs = pd.read_pickle('docs.pkl' if USE_DOCS else 'labeled_posts.pkl', compression='gzip')
docs.dropna(inplace=True)
docs['punc'] = docs['content'].apply(lambda c: re.findall(r"[^\w\s]+", c.lower(), re.UNICODE))

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm
from sklearn import dummy
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import scale
from scipy.stats import spearmanr
from sklearn.metrics import make_scorer
from sklearn.pipeline import Pipeline
import numpy

pipeline = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
])

X = docs['content'].values
y = docs['scum'].values

def my_scorer(y, y_pred, **kwargs):
    print(docs.iloc[numpy.argmax(y_pred)])
    return 0

model = svm.LinearSVC(class_weight='balanced')
# model = dummy.DummyClassifier()
print('hey')
scores = cross_validate(pipeline, X, y, cv=StratifiedKFold(), scoring=['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'])
print(scores)
# cross_validate(model, X, y, cv=StratifiedKFold(), scoring=make_scorer(my_scorer))
print([spearmanr(x, y) for x in X.T])
