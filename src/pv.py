import pandas as pd
import re
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm
from sklearn import dummy
from sklearn.metrics import precision_recall_curve
from sklearn.naive_bayes import MultinomialNB
import numpy

docs = pd.read_pickle('docs.pkl', compression='gzip')
docs.reset_index(inplace=True)
print('hey')
s = Doc2Vec(
    (TaggedDocument(doc['words'], [i]) for i, doc in docs.iterrows()),
    iter=20,
    min_count=5,
    size=300
)
print(len(s.docvecs), len(docs))

model = svm.LinearSVC(class_weight='balanced')
scores = cross_validate(model, s.docvecs, docs['scum'].values.astype('bool'), cv=StratifiedKFold(n_splits=10), scoring=['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'])
print(scores)
