from gensim.models.wrappers import FastText
from gensim.models import KeyedVectors
import gensim
import pandas as pd
import numpy as np
import re
import numpy
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm
from sklearn import dummy
from sklearn.metrics import precision_recall_curve
from sklearn.naive_bayes import MultinomialNB

docs = pd.read_pickle('docs.pkl', compression='gzip')
y = docs['scum'].values

# wv = [("FastText", "wiki"), ("GloVe", "wiki200d"), ("GloVe", "wiki"), ("GloVe", "twitter")]
wv = [("GloVe", "wiki200d")]
for m, dataset in wv:
    s = FastText.load_fasttext_format(dataset+".en") if m == "FastText" else KeyedVectors.load_word2vec_format('w2v.'+dataset+'.txt', binary=False)
    words = np.array([w for w in s.wv.vocab.keys()])
    vectors = [s[w] for w in words]

    X = np.stack(docs['vector_' + m + '_' + dataset].values)
    model = svm.LinearSVC(class_weight='balanced')
    model.fit(X, y)
    conf = model.decision_function(vectors)
    print(conf)
    n = 20
    most_deceptive = words[np.argpartition(conf, -n)[-n:]]
    print(most_deceptive)

    model.decision_function(X)
    

    # print(words[])
