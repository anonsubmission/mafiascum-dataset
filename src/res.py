import pandas as pd
import numpy as np

res = pd.read_pickle('res.pkl')
# print(res)
# res = res[(res['wc'] == 5000) | res['name'].str.contains('FT') | res['name'].str.contains('pick')]
# print(res)
res['AUROC'] = res['scores'].apply(lambda s: round(s['test_roc_auc'], 3))
res['AP'] = res['scores'].apply(lambda s: round(s['test_average_precision'], 3))
res['wc'] = res['wc'].apply(lambda x: x if type(x) == np.str else str(max(x, 50)) + '-' + str(x + 1999))
range_res = res.loc[res['name'] == 'FastText', ['wc', 'N', 'ROC AUC', 'PR']].set_index(['wc'])

res = res.loc[(res['wc'] == 'all'), ['name', 'ROC AUC', 'PR']]

range_res.to_latex('range_results.tex')
res.to_latex('results.tex', index=False)

print(res)
print(range_res)
