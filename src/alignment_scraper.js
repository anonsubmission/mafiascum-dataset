// https://forum.mafiascum.net/viewtopic.php?f=53&t=29549

document.querySelectorAll('span[style*=italic]').forEach(x => x.outerText = x.innerText)
slots = [...document.querySelectorAll("ol > li")].map(x => {
	var p = x.parentNode;
	while (!p.matches('a') && !p.querySelector('a')) p = p.previousElementSibling
	if (p.querySelector('a')) p = p.querySelector('a')
	var users = x.childNodes[0].textContent.replace(/,/g, '').trim().split(/\s+replace.\s+|\s+who replace.\s+/g)
	var role = x.childNodes[1] && x.childNodes[1].textContent
	var event = x.childNodes[2] && x.childNodes[2].textContent.replace(/,/g, '').trim()
	
	return {
		game_id: +p.href.match(/t=[0-9]+/)[0].slice(2),
		text: x.textContent,
		users, role, event
	}
})

// https://forum.mafiascum.net/viewtopic.php?f=53&t=15732

document.querySelectorAll('span[style*=italic]').forEach(x => x.outerText = x.innerText)
slots = []; document.querySelectorAll('.postbody .content a[href*="t="]').forEach(x => {
	var i = x.parentNode.matches('span') ? x.parentNode : x
	while (i && !(i.previousSibling.tagName == 'BR' && (i.previousSibling.previousSibling.textContent == ':' || i.previousSibling.previousSibling.textContent == 'Players'))) i = i.nextSibling
	while (i && i.nodeType == Node.TEXT_NODE) {
		if (i.nextSibling && i.nextSibling.tagName != 'BR') {
			var users = i.textContent.replace(/[0-9]+\.|,/g, '').trim().split(/\s+replace.\s+|\s+who replace.\s+/g)
			var role = i.nextSibling.textContent
			var event = i.nextSibling.nextSibling && i.nextSibling.nextSibling.textContent.replace(/,/g, '').trim()
		}
		else {
			var [usertext, rest] = i.textContent.split(/\s*\(/, 2)
			var [role, rest] = rest ? rest.split(")") : []
			var event = rest && rest.replace(/^\s*-\s*/, "")
			var users = usertext.split(/\s+replace.\s+|\s+who replace.\s+|,\s+/g).map(x => x.replace(/,/g, '').trim())
		}
		slots.push({
			game_id: +x.href.match(/t=[0-9]+/)[0].slice(2),
			text: i.textContent,
			users, role, event
		})
		i = i.nextSibling
		while(i && i.previousSibling.tagName != 'BR') i = i.nextSibling
    }
})