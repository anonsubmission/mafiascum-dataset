import pandas as pd
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm
from sklearn import dummy
from sklearn.metrics import precision_recall_curve
from sklearn.naive_bayes import MultinomialNB
import numpy

docs = pd.read_pickle('glove-twitter-avg.pkl', compression='gzip')
print(docs['scum'].sum() / len(docs))
# print(docs)
# docs['vector'] = docs['scum'].apply(lambda x: numpy.random.rand(300))
model = svm.LinearSVC(class_weight='balanced')
# model = MultinomialNB()
# model = dummy.DummyClassifier()
# avg precision = AU PRC
scores = cross_validate(model, numpy.stack(docs['vector'].values), docs['scum'].values.astype('bool'), cv=StratifiedKFold(n_splits=10), scoring=['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'])
print(scores)
